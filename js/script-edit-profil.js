function chooseField(link_id, field_id) {
  $(`#${link_id}`).hide()
  $(`#${field_id}`).prop('disabled', false)
}
function addressField() {
  $(`#edit-alamat`).hide()
  $(`.box-alamat__shown`).hide()
  $(`.box-alamat__input`).css('display', 'grid')
}
