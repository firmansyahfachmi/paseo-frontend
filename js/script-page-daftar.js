function checkValid(id_h, id) {
  if ($(`#${id}`).is(':invalid')) {
    $(`#${id_h}`).show()
  } else if ($(`#${id}`).is(':valid')) {
    $(`#${id_h}`).hide()
  }
}
