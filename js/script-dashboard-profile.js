// $('#histori-tab').hide()
// $('#undian-tab').hide()
// $('#dashboard-tab').hide()
// $('#hadiah-tab').hide()

function openTabDashboard(event, c) {
  tabcontent = document.getElementsByClassName('tabbed-content')
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].classList.add('dpn')
  }

  tab_dashboard = document.getElementsByClassName('tab-dashboard-group')
  for (i = 0; i < tab_dashboard.length; i++) {
    tab_dashboard[i].className = tab_dashboard[i].className.replace(
      ' tab-dashboard-active',
      ''
    )
  }
  document.getElementById(c).classList.remove('dpn')
  event.currentTarget.className += ' tab-dashboard-active'
}

function changeFilterJenis(event) {
  jenis_filter = document.getElementsByClassName('filter-jenis')
  for (i = 0; i < jenis_filter.length; i++) {
    jenis_filter[i].className = jenis_filter[i].className.replace(
      ' jenis-active',
      ''
    )
  }
  event.currentTarget.className += ' jenis-active'
}

function changeFilterJumlah(event) {
  jenis_jumlah = document.getElementsByClassName('filter-jumlah')
  for (i = 0; i < jenis_jumlah.length; i++) {
    jenis_jumlah[i].className = jenis_jumlah[i].className.replace(
      ' jumlah-active',
      ''
    )
  }
  event.currentTarget.className += ' jumlah-active'
}

// $('.popup-filter-hadiah').hide()
// $('.popup-sortir-hadiah').hide()
// $('.popup-dapat-poin').hide()
// $('.modal-overlay').hide()

$('.filter-btn').click(function () {
  $('.popup-filter-hadiah').show()
  $('.modal-overlay').show()
})
$('.close-popup-filter').click(function () {
  $('.popup-filter-hadiah').hide()
  $('.modal-overlay').hide()
})
$('.btn-submit').click(function () {
  $('.popup-filter-hadiah').hide()
  $('.modal-overlay').hide()
})

$('.sort-btn').click(function () {
  $('.popup-sortir-hadiah').show()
  $('.modal-overlay').show()
})
$('.close-popup-sortir').click(function () {
  $('.popup-sortir-hadiah').hide()
  $('.modal-overlay').hide()
})
$('.btn-submit').click(function () {
  $('.popup-sortir-hadiah').hide()
  $('.modal-overlay').hide()
})

$('.btn-akses-dashboard').click(function () {
  $('.popup-dapat-poin').hide()
  $('.modal-overlay').hide()
})
$('.close-popup-daily').click(function () {
  $('.popup-dapat-poin').hide()
  $('.modal-overlay').hide()
})

///////////////////////////////////// Tour
function startTour() {
  $('.popup-mulai-tur').addClass(' dpn')
  $('.modal-overlay').addClass(' dpn')
  console.log('start tour')
  let tourOptions = {
    options: {
      darkLayerPersistence: true,
      next: 'Selanjutnya',
      prev: 'Sebelumnya',
      finish: 'Selesai',
      of: 'dari',
      mobileThreshold: 100,
    },
    tips: [
      {
        title: 'Point dan Stamp',
        description: 'Lihat jumlah Poin dan Stamp Anda disni, mulai upload',
        image:
          '<%=request.getContextPath()%>/assets/img/paseo-default-category.png',
        selector: '.profile-dashboard__bottom',
        x: 50,
        y: 150,
        offx: 10,
        offy: -20,
        position: 'bottom',
        onSelected: function printLog(e) {
          //console.log('1', e)
          animateTour('.profile-dashboard__bottom', 0)
        },
      },
      {
        title: 'Ini Stamp Meter',
        description: 'Setiap 500 point akan menjadi 1 stamp',
        /*image : 'my/image/path.png',*/
        selector: '.box-stamp-progress-bar',
        x: 50,
        y: 120,
        offx: 10,
        offy: -20,
        position: 'bottom',
        onSelected: function printLog(e) {
          animateTour('.box-stamp-progress-bar', e.currentTip)
        },
      },
      // {
      //     title : 'Undian stamp',
      //     description : 'Yuk kumpulkan Stampnya, untuk perbanyak kesempatan menangkan undian berikut ini!',
      //     /*image : 'my/image/path.png',*/
      //     selector : '.carousel-undian-stamp .slick-active[data-tour-stamp="0"] .card-undian',
      //     x : 50,
      //     y : 50,
      //     offx : 10,
      //     offy : -20,
      //     position : 'bottom',
      //     onSelected : function printLog(e){
      //         animateTour('.carousel-undian-stamp .slick-active[data-tour-stamp="0"] .card-undian', e.currentTip)
      //         $('.carousel-undian-stamp').slick('setPosition')
      //     }

      // },
      // {
      //     title : 'Activity',
      //     description : 'Ikuti activity untuk mendapatkan point',
      //     /*image : 'my/image/path.png',*/
      //     selector : '.carousel-mission .slick-active[data-tour-act="0"] .carousel-mission__cards__body',
      //     x : 50,
      //     y : 50,
      //     offx : 10,
      //     offy : -20,
      //     position : 'bottom',
      //     onSelected : function printLog(e){
      //         animateTour('.carousel-mission .slick-active[data-tour-act="0"] .carousel-mission__cards__body'
      //             , e.currentTip)
      //         $('.carousel-mission').slick('setPosition')
      //     }
      // }
    ],
  }

  ProductTourJS.init(tourOptions)
  ProductTourJS.start()
  ProductTourJS.tips[0].onSelected() //to make first tip onSelected triggered
}

function animateTour(target, currentTip) {
  $('.highlighted').removeClass('highlighted')
  $(target).addClass('highlighted')
  let calculateTop = $(target).eq(0).offset().top - 65 //65 is for header height
  if (currentTip == 0) calculateTop = 0
  $('html, body').animate(
    {
      scrollTop: calculateTop,
    },
    500
  )
}

$(document).on('click', '.product-tour-js-close', function () {
  console.log('close')
  $('.product-tour-js-single-step').remove()
})

$(document).on('click', '.product-tour-js-single-step-final-btn', function () {
  console.log('finish')
  $('.product-tour-js-single-step').remove()
})
