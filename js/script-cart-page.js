/////////////////////////////////////// CART PAGE SCRIPT //////////////////////////////////////////////////
/////

function clearProduk(id) {
  content = document.getElementsByClassName('jumlah-produk-section__group')
  if (content.length > 1) {
    $(`#${id}`).hide()
    $('.btn-tambah-produk').show()
  } else if (content.length == 1) {
    $(`#${id}`).hide()
    $('.btn-tambah-produk').hide()
    $('#brandProduk').show()
  }
}

$('.btn-upload-struk').click(function () {
  $('#jumlah-produk-section').show()
  $('.btn-tambah-produk').show()
  $('#brandProduk').hide()
})

// $('.popup-ketentuan').hide()
// $('.popup-success').hide()
// $('.popup-tambahproduk').hide()
// $('.modal-overlay').hide()
// $('.close-brand-produk').hide()

$('.choose-file-btn').click(function () {
  $('.popup-ketentuan').show()
  $('.modal-overlay').show()
})
$('#paseo-produk').click(function () {
  $('.popup-tambahproduk').show()
  $('.modal-overlay').show()
})
$('.close-popup-ketentuan').click(function () {
  $('.popup-ketentuan').hide()
  $('.modal-overlay').hide()
})
$('.close-popup-tamprod').click(function () {
  $('.popup-tambahproduk').hide()
  $('.modal-overlay').hide()
})
$('.btn-submit').click(function () {
  $('.popup-tambahproduk').hide()
  $('.modal-overlay').hide()
  //   $('.jumlah-produk-section__group').show()
  $('.btn-tambah-produk').show()
  $('#brandProduk').hide()
})
$('.btn-tambah-produk').click(function () {
  $('.btn-tambah-produk').hide()
  $('#brandProduk').show()
  $('.close-brand-produk').removeClass(' dpn')
})
$('.close-brand-produk').click(function () {
  $('.btn-tambah-produk').show()
  $('#brandProduk').hide()
  $('.close-brand-produk').addClass(' dpn')
})

document.getElementById('jenis-1').style.display = 'grid'
function openTab(event, jenis) {
  tabcontent = document.getElementsByClassName('content-side__card')
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = 'none'
  }

  tabjenis = document.getElementsByClassName('tab-jenis')
  for (i = 0; i < tabjenis.length; i++) {
    tabjenis[i].className = tabjenis[i].className.replace(
      ' tab-jenis-active',
      ''
    )
  }
  document.getElementById(jenis).style.display = 'grid'
  event.currentTarget.className += ' tab-jenis-active'
}

/////////////////////////////////////// CART SCRIPT PAGE END //////////////////////////////////
