/////////////////////////////////////// CAROUSEL SCRIPT ////////////////////////////////////////////
///
$('.home-carousel').slick({
  autoplay: true,
  autoplaySpeed: 4000,
  dots: true,
  arrows: true,
  // fade: true
  prevArrow:
    '<button class="slick-previous slick-previous--position-home btn-gold"><i class="icon-chevron-left"></i></button>',
  nextArrow:
    '<button class="slick-nexter slick-nexter--position-home btn-gold"><i class="icon-chevron-right"></i></button>',
})
// $(document).ready(function () {
//   let card_prod = document.getElementsByClassName('.card')
//   let card_show = card_prod.length > 1 ? 3 : 1
// })

$('.section-carousel-product').slick({
  // autoplay: true,
  // autoplaySpeed: 4000,
  dots: false,
  arrows: true,
  slidesToShow: 3,
  slidesToScroll: 3,
  centerMode: true,
  // variableWidth: true,
  
  prevArrow:
    '<button class="slick-previous slick-previous--position btn-gold"><i class="icon-chevron-left"></i></button>',
  nextArrow:
    '<button class="slick-nexter slick-nexter--position-product btn-gold"><i class="icon-chevron-right"></i></button>',
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        centerMode: false,
        variableWidth: false,
      },
    },
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ],
})
$('.box-card-product-self').slick({
  // autoplay: true,
  // autoplaySpeed: 4000,
  // variableWidth: true,
  infinite: true,
  dots: false,
  arrows: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  centerMode: true,
  prevArrow:
    '<button class="slick-previous slick-previous--position btn-gold"><i class="icon-chevron-left"></i></button>',
  nextArrow:
    '<button class="slick-nexter slick-nexter--position btn-gold"><i class="icon-chevron-right"></i></button>',
  responsive: [
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        centerMode: false,
      },
    },
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ],
})
$('.carousel-catalog').slick({
  // autoplay: true,
  // autoplaySpeed: 4000,
  // variableWidth: true,
  dots: false,
  arrows: true,
  slidesToShow: 3,
  slidesToScroll: 1,
  prevArrow:
    '<button class="slick-previous slick-previous--position btn-gold"><i class="icon-chevron-left"></i></button>',
  nextArrow:
    '<button class="slick-nexter slick-nexter--position-catalog btn-gold"><i class="icon-chevron-right"></i></button>',
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        variableWidth: false,
        centerMode:false
      },
    },
  ],
})
$('.carousel-artikel').slick({
  // autoplay: true,
  // autoplaySpeed: 4000,
  // variableWidth: true,
  dots: false,
  arrows: true,
  slidesToShow: 3,
  slidesToScroll: 1,
  prevArrow:
    '<button class="slick-previous slick-previous--position-artikel btn-gold"><i class="icon-chevron-left"></i></button>',
  nextArrow:
    '<button class="slick-nexter slick-nexter--position-artikel btn-gold"><i class="icon-chevron-right"></i></button>',
  responsive: [
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        centerMode: false,
        variableWidth: false,
      },
    },
  ],
})
$('.carousel-socmed').slick({
  // autoplay: true,
  // autoplaySpeed: 4000,
  variableWidth: false,
  centerMode:true,
  dots: false,
  arrows: true,
  slidesToShow: 3,
  slidesToScroll: 3,
  prevArrow:
    '<button class="slick-previous slick-socmed slick-socmed--left btn-gold"><i class="icon-chevron-left"></i></button>',
  nextArrow:
    '<button class="slick-nexter slick-socmed slick-socmed--right btn-gold"><i class="icon-chevron-right"></i></button>',
  responsive: [
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        centerMode: false,
        variableWidth: false,
      },
    },
  ],
})
$('.carousel-mission').slick({
  // autoplay: true,
  // autoplaySpeed: 4000,
  dots: false,
  arrows: true,
  slidesToShow: 3,
  slidesToScroll: 1,
  prevArrow:
    '<button class="slick-previous slick-previous--position btn-gold"><i class="icon-chevron-left"></i></button>',
  nextArrow:
    '<button class="slick-nexter slick-nexter--position-mission btn-gold"><i class="icon-chevron-right"></i></button>',
  responsive: [
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
      },
    },
  ],
})

$('.carousel-pemenang-undian').slick({
  // autoplay: true,
  // autoplaySpeed: 4000,
  variableWidth:false,
  // centerMode:true,
  dots: false,
  arrows: true,
  slidesToShow: 3,
  slidesToScroll: 1,
  prevArrow:
    '<button class="slick-previous slick-previous--position btn-gold"><i class="icon-chevron-left"></i></button>',
  nextArrow:
    '<button class="slick-nexter slick-nexter--position-pemenang btn-gold"><i class="icon-chevron-right"></i></button>',
  responsive: [
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        variableWidth:false,
      },
    },
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        variableWidth:false,
      },
    },
  ],
})

$('.carousel-perjalanan-paseo').slick({
  dots: false,
  arrows: true,
  slidesToShow: 1,
  adaptiveHeight: true,
  prevArrow:
    '<button class="slick-previous slick-previous--position-tentang btn-gold"><i class="icon-chevron-left"></i></button>',
  nextArrow:
    '<button class="slick-nexter slick-nexter--position-tentang btn-gold"><i class="icon-chevron-right"></i></button>',
  responsive: [
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
  ],
})
$('.carousel-undian-stamp').slick({
  dots: false,
  arrows: true,
  slidesToShow: 3,
  slidesToScroll: 1,
  prevArrow:
    '<button class="slick-previous slick-previous--position btn-gold"><i class="icon-chevron-left"></i></button>',
  nextArrow:
    '<button class="slick-nexter slick-nexter--position-undian-stamp btn-gold"><i class="icon-chevron-right"></i></button>',
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
  ],
})
$('.carousel-penghargaan-paseo').slick({
  dots: false,
  arrows: true,
  slidesToShow: 1,
  adaptiveHeight: true,
  prevArrow:
    '<button class="slick-previous slick-previous--position-tentang btn-gold"><i class="icon-chevron-left"></i></button>',
  nextArrow:
    '<button class="slick-nexter slick-nexter--position-tentang btn-gold"><i class="icon-chevron-right"></i></button>',
})
$('.carousel-allproduct').slick({
  // autoplay: true,
  // autoplaySpeed: 4000,
  variableWidth: false,
  centerMode: false,
  dots: false,
  arrows: true,
  slidesToShow: 3,
  slidesToScroll: 1,
  prevArrow:
    '<button class="slick-previous slick-previous--position btn-gold"><i class="icon-chevron-left"></i></button>',
  nextArrow:
    '<button class="slick-nexter slick-nexter--position-relate-produk btn-gold"><i class="icon-chevron-right"></i></button>',
  responsive: [
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        centerMode: false,
        variableWidth: false,
      },
    },
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        variableWidth:false,
      },
    },
  ],
})
/////////////////////////////////////// CAROUSEL SCRIPT END /////////////////////////////////////////////////////
