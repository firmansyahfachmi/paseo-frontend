function onlyNumber(value) {
  return value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')
}
function few_dots(event) {
  let par = event.currentTarget.innerHTML
  let para = par.substring(0, 20) + '...'
}
///////////////////////////////////////////////// Header Script /////
// $('.overlay-backsearch').hide()
// $('.overlay-backsearch__search-box').hide()
// $('.btn-submit-cari-responsive').hide()
// $('.responsive-header').hide()
// $('.btn-nav-respon__icon').hide()
// $('.btn-close-search').hide()

$('.btn-search').click(function () {
  $('.overlay-backsearch').css('opacity', 1)
  $('.overlay-backsearch').css('animation', 'animateSearch 0.2s ease')
  $('.overlay-backsearch').removeClass(' dpn')
  $('.overlay-backsearch__search-box').removeClass(' dpn')
  $('.btn-close-search').removeClass(' dpn')
  $('.btn-close-search').css('opacity', 1)
  $('.btn-close-search').css('animation', 'animateSearch 0.2s')
  $('.btn-search').addClass(' dpn')
  $('.btn-search').css('opacity', 0)
  let element = $(`.dropdown-myprofile`).hasClass('dpn')
  if (element == false) {
    $(`.dropdown-myprofile`).addClass(' dpn')
    $(`.arr-drop-akun`).removeClass(' arr-drop-flip')
  }
})
$('.btn-close-search').click(function () {
  $('.overlay-backsearch').css('opacity', 0)
  $('.overlay-backsearch').css('animation', 'none')
  $('.overlay-backsearch').addClass(' dpn')
  $('.overlay-backsearch__search-box').addClass(' dpn')
  $('.btn-close-search').addClass(' dpn')
  $('.btn-close-search').css('opacity', 0)
  $('.btn-search').removeClass(' dpn')
  $('.btn-search').css('opacity', 1)
  $('.btn-search').css('animation', 'animateSearch 0.2s')
})

function openAkunDrop() {
  let element = $(`.dropdown-myprofile`).hasClass('dpn')
  let element2 = $(`.overlay-backsearch__search-box`).hasClass('dpn')
  if (element2 == false) {
    $('.overlay-backsearch__search-box').addClass(' dpn')
    $('.btn-close-search').addClass(' dpn')
    $('.btn-search').removeClass(' dpn')
  }
  if (element == false) {
    $(`.arr-drop-akun`).removeClass(' arr-drop-flip')
    $(`.dropdown-myprofile`).addClass(' dpn')
    $(`.overlay-backsearch`).addClass(' dpn')
  } else if (element == true) {
    $(`.arr-drop-akun`).addClass(' arr-drop-flip')
    $(`.dropdown-myprofile`).removeClass(' dpn')
    $(`.overlay-backsearch`).removeClass(' dpn')
  }
}

// function showBtnCari() {
//   if ($('.search-input-respond').is(':focus')) {
//     $('.btn-submit-cari-responsive').removeClass(' dpn')
//   } else {
//     $('.btn-submit-cari-responsive').addClass(' dpn')
//   }
// }
function btnNavRespon() {
  if ($('#navi-toggle').is(':checked')) {
    $('.btn-nav-respon__icon-clicked').show()
    $('.btn-nav-respon__icon').addClass(' dpn')
    $('.responsive-header').removeClass(' dpn')
  } else {
    $('.btn-nav-respon__icon-clicked').hide()
    $('.responsive-header').addClass(' dpn')
    $('.btn-nav-respon__icon').removeClass(' dpn')
  }
}

// $('.btn-nav-respon__icon').addClass(' dpn')

$(document).ready(function () {
  if ($(window).width() < 769) {
    $('.btn-nav-respon__icon').removeClass(' dpn')
  } else {
    $('.btn-nav-respon__icon').addClass(' dpn')
  }
})

$(window).resize(function () {
  if ($(window).width() > 768) {
    $('.btn-nav-respon__icon').addClass(' dpn')
  }  else {
    $('.btn-nav-respon__icon').removeClass(' dpn')
    $('.responsive-header').addClass(' dpn')
    $('.btn-nav-respon__icon-clicked').hide()
  }
})

///////////////////////////////////////////////// Header Script END/////

////////////////////////////////// Category Tab

function changeKategori(event, kat) {
  jenis_filter = document.getElementsByClassName('category-tissue__btn')
  for (i = 0; i < jenis_filter.length; i++) {
    jenis_filter[i].className = jenis_filter[i].className.replace(
      ' active-kat',
      ''
    )
  }
  event.currentTarget.className += ' active-kat'

  jenis = document.getElementsByClassName('produk-lain')
  for (i = 0; i < jenis.length; i++) {
    $('.produk-lain').removeClass(' color-nice')
    $('.produk-lain').removeClass(' color-jolly')
    $('.produk-lain').removeClass(' color-toply')
  }

  if (kat == 'nice') {
    $('.produk-lain').addClass(' color-nice')
  } else if (kat == 'jolly') {
    $('.produk-lain').addClass(' color-jolly')
  } else if (kat == 'toply') {
    $('.produk-lain').addClass(' color-toply')
  }
}

//////////////////////////////////// END

/////////////////////////////////// Sitemap

$('#wrap-opener').click(function () {
  let element = $('.wrap-sub').hasClass('dpn')
  if (element == false) {
    $('#wrap-opener').removeClass(' fliped-arr')
    $('.wrap-sub').addClass(' dpn')
  } else if (element == true) {
    $('#wrap-opener').addClass(' fliped-arr')
    $('.wrap-sub').removeClass(' dpn')
  }
})

function openerWrap(opener, wrap) {
  let element = $(`#${wrap}`).hasClass('dpn')
  if (element == false) {
    $(`#${opener}`).removeClass(' fliped-arr')
    $(`#${wrap}`).addClass(' dpn')
  } else if (element == true) {
    $(`#${opener}`).addClass(' fliped-arr')
    $(`#${wrap}`).removeClass(' dpn')
  }
}

////////////////////////// sitemap end

////////////////////////// FAQ

function openQuestion(opener, wrap, question) {
  let element = $(`#${wrap}`).hasClass('dpn')
  let element2 = $(`#${opener}`).hasClass('expand-plus')
  let element3 = $(`#${question}`).hasClass('q-blue')

  if (element == true && element2 == true && element3 == false) {
    $(`#${opener}`).removeClass('expand-plus')
    $(`#${opener}`).addClass('minimize-min')
    $(`#${question}`).addClass(' q-blue')
    $(`#${wrap}`).removeClass(' dpn')
  } else if (element == false && element2 == false && element3 == true) {
    $(`#${opener}`).removeClass('minimize-min')
    $(`#${opener}`).addClass('expand-plus')
    $(`#${wrap}`).addClass(' dpn')
    $(`#${question}`).removeClass(' q-blue')
  }
}
////////////////////////// FAQ END

//////////////////////////// Hasil Cari

function changeKategoriCari(event, target) {
  tabcontent = document.getElementsByClassName('hasil-kat')
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].classList.add('dpn')
  }

  jenis_filter = document.getElementsByClassName('change-kat-s')
  for (i = 0; i < jenis_filter.length; i++) {
    jenis_filter[i].className = jenis_filter[i].className.replace(
      ' active-kat-cari',
      ''
    )
  }

  if (target == 'cari-prod') {
    document.getElementById('cari-title').innerHTML = 'Produk'
  } else if (target == 'cari-artikel') {
    document.getElementById('cari-title').innerHTML = 'Artikel tips'
  }
  document.getElementById(target).classList.remove('dpn')
  event.currentTarget.className += ' active-kat-cari'
}
